import kotlinx.serialization.Serializable

@Serializable
data class Question(
    val numberID: Int,
    val title: String,
    val statement: String,
    val inputMeasures: String,
    val outputMeasures: String,
    val publicInput: String,
    val publicOutput: String,
    val privateInput: String,
    val privateOutput: String,
    var isResolved: Boolean,
    var tries: MutableList<String>
)
