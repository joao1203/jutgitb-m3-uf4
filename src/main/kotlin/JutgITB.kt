import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.io.FileOutputStream

var solvedQuestions: Int = 0
val questionsList = mutableListOf<Question>()
val jsonFile = File("src/main/kotlin/questions.json")
val readJsonFile = jsonFile.readLines()

fun jsonToList(){
    for (i in readJsonFile){
        val obj = Json.decodeFromString<Question>(i)
        questionsList.add(obj)
    }
}

fun resetJson(){
    val questionsLists = listOf<Question>(
        Question(1,"Double the integer!","Write a program that reads an input number and outputs its double.","The input will be an integer number.","The output will be an integer that corresponds to the double value of the input integer.","25","50","33","66", false, mutableListOf()),
        Question(2,"Sum of two integer numbers!","Write a program that when given a set of two integers returns their sum.","The input will be two integer numbers.","The output will be an integer that corresponds to the sum of the input integers.","12 9","21","13 19","32", false, mutableListOf()),
        Question(3,"Calculate the discount!","Write a program that reads the original price, the current price and outputs the discount percentage.","The input will be the two prices, the first one should be the original price followed by the current price.","The output will be a decimal value that corresponds to the discount applied to the price (Float value).","600 450","25.00","800 750","6.25", false, mutableListOf()),
        Question(4,"Is of legal age?","Write a program that reads the input received by the user and checks if the user is of legal age.","The input will be an integer value that represents the users age.","The output will be a boolean value, true if the user is of legal age and false if they are underage.","27","true","12","false", false, mutableListOf()),
        Question(5,"How much time?","Write a program that calculates how many hours, minutes and seconds there are in a given amount of seconds.","The input will be an integer value that corresponds to an amount of seconds","The output will be the number of hours, minutes and seconds that the given amount of seconds equals to.","34532","9 hours 35 minutes 32 seconds","47589","13 hours 13 minutes 9 seconds", false, mutableListOf()),
        Question(6,"Biggest of 3 numbers!","Write a program that when given three numbers, prints the biggest of them.","The input will be the integer numbers.","The output will be the biggest of the previous three integer numbers.","23 57 38","57","43 69 82","82", false, mutableListOf()),
        Question(7,"Even or odd?","Write a program that when given a number prints out if its even or odd.","The input will be an integer number.","The output will be if the given integer is an even or odd number.","12","even","37","odd", false, mutableListOf()),
        Question(8,"Calculator!","Write a program that when given two integer numbers and an operation symbol, executes the operation and returns the result.","The input will be two integer numbers and one of the following symbols (+, -, *, /, %).","The output will be the result of the operation.","23 25 +", "48", "12 4 *", "48", false, mutableListOf()),
        Question(9,"How many days does the month have?","Write a program that when given a number that corresponds to one of the months of the year responds with the amount of days in said month.","The input will be an integer value.","The output will be the amount of days in the chosen month.","3","31","2","28", false, mutableListOf()),
        Question(10,"Absolute value!","Write a program that when given a number returns its absolute value.","The input will be an integer number.","The output will be the absolute value of the given integer","-34","34","57","57", false, mutableListOf()),
        Question(11,"Print the range!","Write a program that when given two integer numbers prints out the sequence between the two of them.","The input will be two integer numbers.","The output will be the sequence of numbers between the integer numbers, all within the same line.","2 7","2,3,4,5,6,7","25 19","25,24,23,22,21,20,19", false, mutableListOf()),
        Question(12,"Raise it!","Write a program that when given two integer numbers prints out the result of the first one raised by the power of the second.","The input will be two integer numbers.","The output will be the result of raising the first by the power of the second.","2 3", "8","5 3","125", false, mutableListOf()),
        Question(13,"Reverse of the integer!","Write a program that when given an integer writes it in reverse.","The input will be an integer number.","The output will be another integer that is equal to the reverse of the integer that the program received.","356","653","147","741", false, mutableListOf()),
        Question(14,"Is it prime?","Write a program that when given an integer number returns if its a prime number or not.","The input will be an integer number.","The output will be whether the integer number is a prime number or not.","14","not prime","3","prime", false, mutableListOf()),
        Question(15,"Extremes!","Write a program that when given a list of numbers returns the biggest and smallest numbers inside it.","The input will be a list of numbers that ends with 0.","The output will be the biggest number followed by the smallest (without taking in account the 0).","15 -23 2 57 89 6 3 0","89 -23","2 6 -1 9 -5 15 -230 16 0","16 -230", false, mutableListOf()),
        Question(16,"Sum of numbers!","Write a program that when given a series of numbers returns the sum of them all.","The input will be a list of integer numbers.","The output will be the sum of all integers.","1 7 3 2 4 7 5 8 7","44","-3 10 1 -2 8","14", false, mutableListOf()),
        Question(17,"Reverse the array!","Write a program that when given an array of integer numbers returns its inverted version.","The input will be an array of integer numbers.","The output will be the inverted array.","5 4 123 345 65 324 1 2 4 66","66 4 2 1 324 65 345 123 4 5","0 9 8 7 6 5 4 3 2 1","1 2 3 4 5 6 7 8 9 0", false, mutableListOf()),
        Question(18,"Which one is missing?","Write a program that when given a sequence of integer numbers returns the number that is missing.","The input will be an ordered sequence of integer numbers.","The output will be the missing number of the sequence.","4 5 7 8 9 10 11","6","1 2 3 4 5 6 7 9 10 11","8",false, mutableListOf()),
        Question(19,"Are they the same?","Write a program that when given two words returns if they are equal or not.","The input will be two words.","The output will be whether the words are equals or not.","hey hay","not equals","bye bye","equals",false, mutableListOf()),
        Question(20,"Palindrome!","Write a program that when given a word returns if its a palindrome or not.","The input will be a word.","The output will be whether the word is a palindrome or not.","wow","yes","damn","no",false, mutableListOf())
    )
    jsonFile.writeText("")
    for (item in questionsLists){
        val toJson = Json.encodeToString<Question>(item)
        FileOutputStream("src/main/kotlin/questions.json", true).bufferedWriter().use { writer ->
            writer.write(toJson + "\n")
        }
    }
}

fun main(){
    loginUser()
}

fun loginUser(){
    println("${underline}${bold}Welcome to JutgITB 2.0!${reset}")
    println("Identify yourself:")
    println(" ·Student\n ·Teacher")
    do {
        val userEnter = sc.next().uppercase()
        when(userEnter){
            "STUDENT" -> studentMenu()
            "TEACHER" -> teacherLogin()
            else -> println("$userEnter is not a valid option, try again!")
        }
    }while (userEnter != "STUDENT" && userEnter != "TEACHER")
}

fun studentMenu(){
    println("${underline}${bold}Student Menu${reset}")
    println(" 1·Follow the learning itinerary\n" +
            " 2·Question List\n" +
            " 3·Check solved questions\n" +
            " 4·Help\n" +
            " 5·Exit")
    while (true){
        when (val sChoice = sc.nextInt()){
            1 -> solveQuestions()
            2 -> checkList()
            3 -> checkSolved()
            4 -> giveHelp()
            5 -> exit()
            else -> println("$sChoice is not a valid option, try again!")
        }
    }
}

fun solveQuestions(){
    jsonToList()
    sc.nextLine()
    for (q in readJsonFile.indices){
        if (!questionsList[q].isResolved){
            Questions().printQuestion(q)
            println("  ${underline}Would you like to solve this question?${reset}\n   1.Yes\n   2.No")

            do {
                var userChoice = sc.nextLine()
                if (userChoice == "1"){
                    Questions().resolveQuestion(q)
                    Questions().printState(q)
                    userChoice = "2"
                }
                else if (userChoice == "2"){
                    if (q != readJsonFile.size-1){
                        println("Understood! Next question incoming!!!\n")
                    }
                }
                else {println("  $userChoice is not a valid option, try again!")}
            }while (userChoice != "2")
        }
    }

    for (i in readJsonFile.indices){
        if (questionsList[i].isResolved == true){
            solvedQuestions++
        }
    }
    println("\n  ${bold}You've solved $solvedQuestions out of ${readJsonFile.size} questions!${reset}")
}

fun checkList(){
    jsonToList()
    for (q in readJsonFile.indices){
        println("${bold}Question ${questionsList[q].numberID}:${reset} ${questionsList[q].title}")
    }
    val userChoice = sc.nextInt()-1
    sc.nextLine()
    questionsList[userChoice].isResolved = false
    Questions().printQuestion(userChoice)
    Questions().resolveQuestion(userChoice)
    Questions().printState(userChoice)
    exit()
}

fun checkSolved(){
    jsonToList()
    val checkedQuestions = mutableListOf<Int>()
    for (q in questionsList.indices){
        if (questionsList[q].tries.size > 0){
            println("Question ${questionsList[q].numberID}: ${questionsList[q].title}")
            Questions().printState(q)
            checkedQuestions.add(questionsList[q].numberID)
        }
    }
    val userChoice = sc.nextInt()
    sc.nextLine()
    if (userChoice in checkedQuestions){
        questionsList[userChoice].isResolved = false
        Questions().printQuestion(userChoice-1)
        Questions().resolveQuestion(userChoice-1)
        Questions().printState(userChoice-1)
    }
    else exit()
}

fun giveHelp(){
    println("empty test")
    println("\nWould you like to resume solving the questions?")
    println(" 1·Yes\n 2·No")
    val choice = sc.nextInt()
    if (choice == 1){
        solveQuestions()
    }
    else exit()
}

fun exit(){
    kotlin.system.exitProcess(0)
}

fun teacherLogin(){
    var attempts = 3
    var password: String
    println("Insert teachers password:")
    do {
        password = sc.next()
        if (password != "itb.cat"){
            attempts--
        }
    }while (attempts > 0 && password != "itb.cat")

    if (attempts == 0){
        println("Out of attempts!")
        exit()
    }
    if (password == "itb.cat"){
        teacherMenu()
    }
}

fun teacherMenu(){
    println("${underline}${bold}Teacher Menu${reset}")
    println(" 1·Add new problems\n" +
            " 2·Review students progress\n" +
            " 3·Exit")
    while (true){
        when (val teacherEnter = sc.nextInt()){
            1 -> addQuestion()
            2 -> gradesNview()
            3 -> exit()
            else -> println("$teacherEnter is not a valid option, try again!")
        }
    }
}

fun addQuestion(){
    sc.nextLine()
    println("Write the title of the new question:")
    val titleNew = sc.nextLine()
    println("Write the statement of the new question:")
    val statNew = sc.nextLine()
    println("Write the input measure of the new question:")
    val inputNew = sc.nextLine()
    println("Write the output measure of the new question:")
    val outputNew = sc.nextLine()
    println("Write the public input of the new question:")
    val publicInput = sc.nextLine()
    println("Write the public output of the new question:")
    val publicOutput = sc.nextLine()
    println("Write the private input of the new question:")
    val privateInput = sc.nextLine()
    println("Write the private output of the new question:")
    val privateOutput = sc.nextLine()

    var questionNum = 1

    jsonFile.forEachLine { questionNum++ }

    val newQuestion = Question(questionNum++,titleNew,statNew,inputNew,outputNew,publicInput,publicOutput,privateInput,privateOutput,false,
        mutableListOf())
    val toJson = Json.encodeToString<Question>(newQuestion)
    FileOutputStream("src/main/kotlin/questions.json", true).bufferedWriter().use { writer ->
        writer.write(toJson + "\n")
    }

}

fun gradesNview(){
    println("${bold}Review students progress!${reset}")
    println(" 1·Get students score\n" +
            " 2·Penalize student\n" +
            " 3·Show progress\n" +
            " 4·Exit")
    while (true){
        when(val userEnter = sc.nextInt()){
            1 -> getStuScore()
            2 -> penalizeStudent()
            3 -> showResults()
            4 -> exit()
            else -> println("$userEnter is not a valid option, try again!")
        }
    }
}

fun getStuScore(){
    jsonToList()
    var stuScore = 0.0
    for (i in questionsList.indices){
        if (questionsList[i].isResolved){
            stuScore += 1.0
            solvedQuestions++
        }
    }
    stuScore /= questionsList.size
    stuScore *= 10
    println("The student solved $solvedQuestions out of ${readJsonFile.size} questions, their grade is $stuScore")
}

fun penalizeStudent(){
    jsonToList()
    var stuScore = 0.0
    for (i in questionsList.indices){
        if (questionsList[i].isResolved){
            stuScore += when(questionsList[i].tries.size){
                1 -> 1.0
                2 -> 0.90
                3 -> 0.80
                4 -> 0.70
                5 -> 0.60
                6 -> 0.50
                else -> 0.25
            }
            solvedQuestions++
        }
    }
    stuScore /= questionsList.size
    stuScore *= 10
    println("The student solved $solvedQuestions out of ${readJsonFile.size} questions, and after penalization their grade is $stuScore")
}

fun showResults(){
    jsonToList()
    for (q in questionsList.indices){
        if (questionsList[q].tries.size > 0){
            println("Question ${questionsList[q].numberID}: ${questionsList[q].title}")
            println("Tries:${questionsList[q].tries.size}")
            println("Solved:${questionsList[q].isResolved}")
        }
    }
}