import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.util.Scanner

val sc = Scanner(System.`in`)
const val reset = "\u001B[0m"
const val bold = "\u001b[1m"
const val underline = "\u001b[4m"
const val framed = "\u001b[51m"
const val magenta = "\u001b[35;1m"
const val cyan = "\u001b[36;1m"
const val red = "\u001b[31;1m"
const val yellow = "\u001b[33;1m"
const val green = "\u001b[32m"
const val brightGreen = "\u001b[32;1m"

@Serializable
class Questions{
    init {
        for (i in readJsonFile){
            val obj = Json.decodeFromString<Question>(i)
            questionsList.add(obj)
        }
    }

    fun printQuestion (q: Int){
        println("${underline}${bold}Question ${questionsList[q].numberID}: ${questionsList[q].title}$reset \n")
        println(" ${red}${framed} Description: ${reset}\n   ${questionsList[q].statement}")
        println("\n ${magenta}${framed} Input: ${reset}\n   ${questionsList[q].inputMeasures}")
        println("\n ${cyan}${framed} Output: ${reset}\n   ${questionsList[q].outputMeasures}")
        println("\n ${yellow}${framed} Example: $reset")
        println("   Input:  ${questionsList[q].publicInput}")
        println("   Output: ${questionsList[q].publicOutput}\n")
    }

    fun resolveQuestion (q: Int){
        println("\n ${green}${framed} Private Playground: $reset")
        println("   Input: ${questionsList[q].privateInput}")
        println("   Output: ${bold}???${reset}")
        do {
            println("\n  Write the correct output:")
            val userAnswer = sc.nextLine()

            if (userAnswer != "cancel"){
                questionsList[q].tries
                questionsList[q].tries.add(userAnswer)
                updateFile(questionsList[q].numberID,questionsList[q].tries, false)
            }
            if (questionsList[q].privateOutput == userAnswer.lowercase()) {
                questionsList[q].isResolved = true
                updateFile(questionsList[q].numberID,questionsList[q].tries, true)
                println("\n  ${bold}Congratulations, $userAnswer is the correct output!$reset")
            }
            else println("  I'm sorry, that is not the correct output...")

        } while (!questionsList[q].isResolved && userAnswer != "cancel")
    }

    fun printState(q: Int){
        println("\n  Number of tries: ${questionsList[q].tries.size}")

        if (!questionsList[q].isResolved){
            println("  ${red}${framed} User Attempts: $reset")

            for (item in questionsList[q].tries.indices){
                println("  ${red}->${reset} ${questionsList[q].tries[item]}")
            }
            println("  Solved: ${questionsList[q].isResolved.toString().uppercase()}\n")
        }
        else {
            println("  ${brightGreen}${framed} User Attempts: $reset")

            for (item in questionsList[q].tries.indices){
                println("  ${brightGreen}->${reset} ${questionsList[q].tries[item]}")
            }
            println("  Solved: ${questionsList[q].isResolved.toString().uppercase()}\n")
        }
    }

    private fun updateFile(id: Int, userTries: MutableList<String>, finish: Boolean){
        val jsonQuest = jsonFile.readLines()
        jsonFile.writeText("")
        for (i in jsonQuest.indices){
            val reformedQuest = Json.decodeFromString<Question>(jsonQuest[i])
            if (reformedQuest.numberID == id){
                reformedQuest.isResolved = finish
                reformedQuest.tries = userTries
            }
            val objModified = Json.encodeToString(reformedQuest)
            jsonFile.appendText("$objModified\n")
        }
    }
}