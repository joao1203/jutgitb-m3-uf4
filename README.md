<h1><strong>M03 Project: JutgITB</strong></h1>
<h3>This project consists of a program used to check the result of codes written by the students of ITB, as of version 1.0 there are only 5 questions and 1 specific answer for each.</h3>
<h2>How to Install and Run the Project:</h2>
<ol>
    <li>Click on the Download Button located next to the Clone option on the repository</li>
    <li>Select the zip option</li>
    <li>Extract the zip file</li>
    <li>Now open IntelliJ IDEA</li>
    <li>Select the File tab located on the top left of your screen</li>
    <li>Select the Open option</li>
    <li>Now go to where you downloaded the project and click OK on the bottom right</li>
    <li>After that you will have to wait awhile for the Project to build itself</li>
    <li>In order to use the judge you will have to open the folders in the following order<br>JugtITBParte1 > src > main > kotlin > JutgITB.kt</li>
</ol>
<h2>How to use:</h2>
<ol>
    <li>To load the judge click on the green play button located on the top right corner</li>
    <li>The console tab will pop up, and the first question will appear</li>
    <li>The user will be able to choose between trying to solve the current question (by typing 1) or go to the next one (by typing 2)</li>
</ol>

<h2>Creator and License:</h2>
<h3>Developed by Joao Victor Lopes Dias and Distribuited with the General Public License Version 2</h3>